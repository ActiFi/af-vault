## Af-vault

## Usage

```npm install gitlab:ActiFi/af-vault```

### Basic Example

```javascript
const afVault = require('af-vault');

let myAppConfig = {
  foo:'blarg',
  hamburger:{}
};

let secretsToLoad = [
  { 
    key:'awsSecretKey1', 
    region:'aswRegion'
  },
  { 
    key:'awsSecretKey2',
    region:'aswRegion',
    scope:'hamburger' // option (all values will be nested into hamburger) 
  }
];

afVault.loadSecretsIntoConfig(secretsToLoad, myAppConfig) // will merge values
  .then(function(updatedConfig){
    myAppConfig = updatedConfig;
  })
  .catch(function(err){
    // something blew up.
  })

```


### Methods
```javascript
// get a secret
afVault.getSecret(key, region)
  .then(function(secret){ 
    //... 
  })
```
```javascript
// get a bunch of secrets... and optionally merge into an existing config
afVault.loadSecrets(listOfScrets, configToMerge)
  .then(function(secret){ 
    //... 
  })
```
```javascript
// get a bunch of secrets... and optionally merge into an existing config
afVault.generateDbUrl(engine, username, password, host, port, db)
  .then(function(url){ 
    // eg url = mysql://myuser:abc123@somesql.cexjmmsqltjg.us-east-1.rds.amazonaws.com:3306/myDB
  })
```