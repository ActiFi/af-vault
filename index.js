
const _ = require('lodash');
const async = require('async');
const AWS = require('aws-sdk');


// create aws mangers once..
let managers = {};
function getAwsManager(region){
  if(!region)
    return null;
  if(managers[region])
    return managers[region];
  managers[region] = new AWS.SecretsManager({ region: region });
  return managers[region]
}


function getSecret(key, region){
  return new Promise(function(resolve, reject){
    let manager = getAwsManager(region);
    if(!manager)
      return reject(new Error('Failed to load secret. Secret manager failed to initialize based on region.'));

    console.log(`Loading secret for key [${key}] in region [${region}]`);

    manager.getSecretValue({ SecretId:key }, function(err, data) {

      if (err)
        return reject(err);

      let secret = null;

      if ('SecretString' in data) {
        secret = data.SecretString;
      } else {
        let buff = new Buffer(data.SecretBinary, 'base64');
        secret = buff.toString('ascii');
      }

      try{
        secret = JSON.parse(secret);
        resolve(secret)
      } catch(err){
        reject(err);
      }

    });

  })
}


function loadSecrets(secretList, configToMerge){

  if(!configToMerge)
    configToMerge = {};

  return new Promise(function(resolve, reject){

    async.eachLimit(secretList, 5, function(secretInfo, next){

      if(!secretInfo.key)    return next('Secret did not contain a key');
      if(!secretInfo.region) return next('Secret did not contain a region.');

      getSecret(secretInfo.key, secretInfo.region)
        .then(function(info){

          // apply all keys to config
          _.each(info, function(value, key){
            let path = key;
            if(secretInfo.scope)
              path = `${secretInfo.scope}.${key}`;
            _.set(configToMerge, path, value);
          });

          next();
        })
        .catch(next);
    },
    function(err) {
      if(err) return reject(err);
      return resolve(configToMerge);
    })

  })
}


function generateDbUrl(engine, username, password, host, port, db){
  return `${engine}://${username}:${password}@${host}:${port}/${db}`;
}

// TBD
// function generateMongoDbUrl(engine, username, password, host, port, db){
//   return `${engine}://${username}:${password}@${host}:${port}/${db}`;
// }


// pass a config with a list of secrets to load...
module.exports = {
  loadSecrets:loadSecrets,
  getSecret:getSecret,
  generateDbUrl:generateDbUrl
};